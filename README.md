**React-app**
An example of data visualization(loaded from a .csv) with React, D3 and Victory.js

**Install steps:**

**-Clone repository to the local environment**

**-Install dependencies:**
Execute npm install to install dependencies

**-Test application**
Execute npm start to test the application in the local environment
