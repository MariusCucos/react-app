import React, { useState, useEffect } from "react";
import { csv } from "d3";
import { VictoryBar, VictoryChart, VictoryTooltip } from "victory";
import Select from "react-select";
import csvFile from "./data.csv";

const App = () => {
    //assigning functions for setting the state 
    const [data, setData] = useState([]);
    const [options, setOptions] = useState([]);
    const [filter, setFilter] = useState("lifeSatisfaction");
    
    //react hook
    useEffect(() => {
        //parsing data from given CSV
        csv(csvFile,changeValuesToInt).then(csvData => {
            //eliminating unnecessary index via deconstructing the csv data
            const headers = Object.keys(csvData[0]);
            const [country, ...validEntries] = headers;

            //setting a 'options' variable for the select
            const options = validEntries.map(item => {
                return { key: item, value: item, label: item };
            });
            
            //setting the component's state
            setOptions(options);
            setData(csvData);
        });
    }, []);

    //changing CSV's values from strings to numbers
    const changeValuesToInt = targetData => {
        const resultData = Object.fromEntries(
            Object.entries(targetData).map(([key, value]) => {
                return key==='country' ? [key, value] : [key, parseFloat(value)];
            })
        );
        return resultData;
    };

    //rendering components
    return ( 
        <div className="mainContainer">
            <div className="selectWrapper">
                <h3 className="column labelColumn">Choose Index:</h3>
                <Select className="column selectColumn"
                    defaultValue={{ label: [filter], value: [filter] }}
                    options={options}
                    onChange={selectedOption => setFilter(selectedOption.value)}
                />
            </div>
            <VictoryChart 
                animate={{ duration: 800 }}
                domainPadding={15}
                width={960}
                height={800}
                padding={{ top: 20, bottom: 50, left: 130, right: 30 }}>

                <VictoryBar
                    style={{ data: { fill: "#f05e56" }, labels: {fontSize:22} }}
                    data={data}
                    barWidth={14}
                    horizontal={true}
                    x="country" y={filter}
                    labels={(data) => Number.isInteger(data._y) ? data._y : data._y.toFixed(2)} labelComponent={<VictoryTooltip />}
                />
                
            </VictoryChart>
        </div>
    );
};

export default App;
